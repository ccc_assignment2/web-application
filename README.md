# Flow diagram
## Version 1
Here is our [Flow diagram](https://www.dropbox.com/s/3ityk0euq1g9t2k/Flow%20Diagram%28edited-Nitro%29.pdf?dl=0)

## Version 2
After reading some research on MapReduce, I feel like this needs to be changed. MapReduce, as far as my preliminary reading has led me to believe, is just a way of organising data. 
[MapReduce: Simplified Data Processing on Large Clusters](https://static.googleusercontent.com/media/research.google.com/en//archive/mapreduce-osdi04.pdf)
From page 37 of the following presentation explains about MapReduce in CouchDB. 
Note that I'm still working on my other assignment and am as yet to work out whether this works. Will know more tomorrow evening.
[CouchDB: JSON,HTTP & MapReduce](http://nyphp.org/resources/nosql-php-couchdb-json-http-mapreduce.pdf)

Here is the [Second Flow diagram](https://www.dropbox.com/home/COMP90050?preview=Flow+Diagram+v2.pdf)

### Notes on MapReduce
'In practice, we choose the amount of map tasks so that each individual task is roughly 16MB to 64MB of input data, and we make the reduce task a small multiple of the number of worker machines we expect to use'
(MapReduce: Simplified Data Processing on Large Clusters)

# WebApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Data visualisation

While reading a research paper, I stumbled accross [D3.js](https://d3js.org/) which is a Browser-Based JavaScript library for visualisation.
The paper states that 'a browser's ability to deal with large datasets is limited - in practice a few megabytes at the most', which means we will need to aggregate the data before we visualise it.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
